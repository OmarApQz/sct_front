import SideBar from "./SidebarPlugin";

import "bootstrap/dist/css/bootstrap.css";
import "../assets/sass/paper-dashboard.scss";

export default {
  install(Vue) {
    Vue.use(SideBar);
  }
}