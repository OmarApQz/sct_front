export default {
  name: "login",
  data() {
    return {
      form: {
        password: "",
        username: "",
      },
      userMock: {
        user : "admin",
        pass : "b052"
      },
      show: true
    }
  },
  methods: {
    onSubmit(evt) {
      evt.preventDefault();
      if(this.form.username != "" && this.password !=""){
        if (this.form.username == this.$parent.mockAccount.username && this.form.password == this.$parent.mockAccount.password) {
          console.log("Usuario y contraseña correctos");
          this.$emit("authenticated",true);
          this.$router.replace({name:"Home"})
        }else{
          alert("Usuario y contraseña incorrectos");
        }

      }
    },
    onReset(evt) {
      evt.preventDefault();
      // Reset our form values
      this.form.password = "";
      this.form.username = "";
      // Trick to reset/clear native browser form validation state
      this.show = false;
      this.$nextTick(() => {
        this.show = true;
      });
    }
  }
};