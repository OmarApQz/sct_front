import Vue from 'vue'
import VueRouter from 'vue-router'
import DashboardLayout from "../layout/dashboard/DashboardLayout.vue";

import Home from "../views/Home"
import Login from "../views/Login"
import ERSchemaEditor from '../views/ERSchemaEditor'
import RelationalSchema from '../views/RelationalSchema'
import NoRelationalSchema from '../views/NoRelationalSchema'

Vue.use(VueRouter)

export default new VueRouter({
  routes: [
    {
      path: "/",
      component: DashboardLayout,
      redirect: "/Home",
      children: [
        // {
        //   path: "Login",
        //   name: "Login",
        //   component: Login
        // },
        {
          path: "Home",
          name: "Home",
          component: Home
        },
        {
          path: 'ERSchemaEditor',
          name: 'ERSchemaEditor',
          component: ERSchemaEditor
        },
        {
          path: 'RelationalSchema',
          name: 'RelationalSchema',
          component: RelationalSchema
        },
        {
          path: 'NoRelationalSchema',
          name: 'NoRelationalSchema',
          component: NoRelationalSchema
        }]
    },
    {
      path: "/login",
      name: "Login",
      component: Login
    },
  ],
  linkActiveClass: "active",
  mode: 'history'
})
